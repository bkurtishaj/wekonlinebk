from django import forms
from .models import SignUp

class SignUpForm(forms.ModelForm):

  full_name = forms.RegexField(required = False, max_length = 20, min_length = 5, regex = r'^[a-zA-Z ]+$', error_messages = { 'invalid' : 'Only alpha characters are allowed'})

  class Meta:
    model = SignUp
    fields = ['full_name','email']

  def clean_full_name(self):
    full_name = self.cleaned_data['full_name']
    if 'bad' in full_name:
      raise forms.ValidationError('Names must not contain profanity')
    return full_name
