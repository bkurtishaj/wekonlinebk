from django.contrib import admin

# Register your models here.
from .models import SignUp
from .forms import SignUpForm

@admin.register(SignUp)
class SignUpAdmin(admin.ModelAdmin):
  date_hierarchy = 'created_time'
  list_display = ('email', 'full_name', 'created_time')
  form = SignUpForm
